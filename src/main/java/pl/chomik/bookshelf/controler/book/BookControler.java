package pl.chomik.bookshelf.controler.book;

import org.hibernate.validator.constraints.ISBN;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import pl.chomik.bookshelf.domain.Bibs;
import pl.chomik.bookshelf.domain.Book;
import pl.chomik.bookshelf.service.Book.BookService;

import java.io.IOException;
import java.net.URISyntaxException;

@Controller
public class BookControler {

    @Autowired
    Book book;

    @Autowired
    BookService bookService;



    @RequestMapping("/")
    public String home() {
        return "index";
    }

    @RequestMapping("/serachBookByISBN.html")
    public String serachBookISBN() {
        return "serachBookByISBN";
    }

    @RequestMapping("/resultPage")
    public String displayBook(BookForm bookForm) {
        return "resultPage";
    }

    @RequestMapping("/booksrelated/displayBook")
    public String displayBook(@RequestParam(name = "ISBN")String str, Model model) throws IOException, URISyntaxException {

        Book book = bookService.serachBook(str);
        Bibs[] bibs = book.getBibs();

        model.addAttribute("autor", bibs[0].getAuthor());
        model.addAttribute("title", bibs[0].getTitle());
        model.addAttribute("id", bibs[0].getId());
        model.addAttribute("ISBN", bibs[0].getIsbnIssn());
        return "booksrelated/displayBook";
    }

    @RequestMapping(name = "/getBook", method = RequestMethod.GET)
    public String getBook(@RequestParam(name = "ISBN") String str, Model model) throws IOException, URISyntaxException {

        Book book = bookService.serachBook(str);


        System.out.println(str + " wartość isbnISSN");
        model.addAttribute("book", book);
        System.out.println(" book: " + book);

        return "booksrelated/displayBook";
    }

    @RequestMapping("/addBook")
    public String addBook(@RequestParam(name = "ISBN")String str) throws IOException, URISyntaxException {

        Book book = bookService.serachBook(str);
        bookService.addBook(book);
        return ("index");
    }

    @RequestMapping("/booksrelated/displayBookDetails")
    public String displayBookDetails(@RequestParam("ISBN") String str, Model model) throws IOException, URISyntaxException {

        Book book1 = bookService.serachBook(str);

        Bibs[] bibs = book1.getBibs();

        model.addAttribute("a", bibs[0].getAuthor());
        model.addAttribute("title", bibs[0].getTitle());
        model.addAttribute("id", bibs[0].getId());
        model.addAttribute("isbn", bibs[0].getIsbnIssn());
        model.addAttribute("fow", bibs[0].getFormOfWork());
        model.addAttribute("orginLanguage", bibs[0].getLanguageOfOriginal());
        model.addAttribute("publicationYear", bibs[0].getPublicationYear());
        model.addAttribute("publisher", bibs[0].getPublisher());
        model.addAttribute("language", bibs[0].getLanguage());


        return "booksrelated/displayBookDetails";


}}
