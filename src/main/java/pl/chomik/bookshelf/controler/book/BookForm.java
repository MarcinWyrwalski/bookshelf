package pl.chomik.bookshelf.controler.book;

import org.hibernate.validator.constraints.ISBN;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class BookForm {

    @NotNull
    @Size(min = 10, max = 13)
    private ISBN isbn;

    public BookForm() {
    }

    public ISBN getIsbn() {
        return isbn;
    }

    public void setIsbn(ISBN isbn) {
        this.isbn = isbn;
    }
}
