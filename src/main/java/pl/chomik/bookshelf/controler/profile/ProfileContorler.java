package pl.chomik.bookshelf.controler.profile;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

@Controller
public class ProfileContorler {

    @RequestMapping("/profile")
    public String displayProfile() {
        return "profile/profilePage";

    }
    @RequestMapping("/sidePage")
    public String displayMainpage(){
        return "/sidePage";
    }

    @RequestMapping(value = "/profile", method = RequestMethod.POST)
    public String saveProfile(BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "profile/profilePage";
        }
        System.out.println("write succesfull" );
        return "redirect:/profile";

    }

    @RequestMapping("/signing")
    public String displayLogin() {
        return "signing";
    }

    @RequestMapping("/profile/userPage")
    public String displayUserPage(){
        return "profile/userPage";
    }
    @RequestMapping("/logout")
    public String logoutPage(){
        return "logout";
    }
}
