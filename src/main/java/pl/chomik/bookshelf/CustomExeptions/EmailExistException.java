package pl.chomik.bookshelf.CustomExeptions;

public class EmailExistException extends Exception {

    String str;

    public EmailExistException(String message, String str) {
        super(message);
        this.str = str;
    }

    public EmailExistException(String s) {

    }

    @Override
    public String toString() {
        return "EmailExistException{" +
                "str='" + str + '\'' +
                '}';
    }
}
