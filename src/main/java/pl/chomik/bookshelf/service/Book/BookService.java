package pl.chomik.bookshelf.service.Book;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import pl.chomik.bookshelf.Persistance.BibsRepository;
import pl.chomik.bookshelf.Persistance.BookRepositoryImpl;
import pl.chomik.bookshelf.domain.Bibs;
import pl.chomik.bookshelf.domain.Book;

import javax.transaction.Transactional;
import java.io.IOException;
import java.net.URISyntaxException;

@Controller
public class BookService {

    @Autowired
    BookRepositoryImpl bookRepository;

    @Autowired
    BibsRepository bibsRepository;


    public Book serachBook(String str) throws URISyntaxException, IOException {
        ObjectMapper mapper = new ObjectMapper();

        URIBuilder builder = new URIBuilder("http://data.bn.org.pl/api/bibs.json");
        builder.addParameter("isbnIssn", str);
        System.out.println(builder.toString());


        Book book = mapper.readValue(builder.build().toURL(), Book.class);

        return book;
    }
    @Transactional
    public void addBook(Book book) {
        Bibs bibs = new Bibs();
        Bibs[] bibs1 = book.getBibs();

        bibs.setAuthor(bibs1[0].getAuthor());
        bibs.setFormOfWork(bibs1[0].getFormOfWork());
        bibs.setGerne(bibs1[0].getGerne());
        bibs.setIsbnIssn(bibs1[0].getIsbnIssn());
        bibs.setLanguage(bibs1[0].getLanguage());
        bibs.setLanguageOfOriginal(bibs1[0].getLanguageOfOriginal());
        bibs.setPublicationYear(bibs1[0].getPublicationYear());
        bibs.setPublisher(bibs1[0].getPublisher());
        bibs.setTitle(bibs1[0].getTitle());



        bibsRepository.save(bibs);


    }
}
