package pl.chomik.bookshelf.Persistance;

import org.springframework.data.repository.CrudRepository;
import pl.chomik.bookshelf.domain.Book;

public interface BookRepository extends CrudRepository<Book, Long> {
}
