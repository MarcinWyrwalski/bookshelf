package pl.chomik.bookshelf.Persistance;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.chomik.bookshelf.domain.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {


}
