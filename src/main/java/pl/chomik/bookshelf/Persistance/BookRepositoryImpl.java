package pl.chomik.bookshelf.Persistance;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import pl.chomik.bookshelf.domain.Book;

import javax.persistence.EntityManager;
import java.util.Optional;

public class BookRepositoryImpl implements BookRepository
{
    @Autowired
    private EntityManager entityManager;


    @Override
    public <S extends Book> S save(S s) {
        entityManager.persist(s);
        return null;
    }

    @Override
    public Iterable saveAll(Iterable iterable) {
        return null;
    }

    @Override
    public Optional<Book> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public Iterable<Book> findAll() {
        return null;
    }

    @Override
    public Iterable findAllById(Iterable iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteAll(Iterable iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(Book book) {

    }
}
