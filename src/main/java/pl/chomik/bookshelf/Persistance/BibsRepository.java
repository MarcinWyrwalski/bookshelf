package pl.chomik.bookshelf.Persistance;

import org.springframework.data.repository.CrudRepository;
import pl.chomik.bookshelf.domain.Bibs;

public interface BibsRepository extends CrudRepository<Bibs, Long> {
}
