package pl.chomik.bookshelf.Persistance;

import org.springframework.beans.factory.annotation.Autowired;
import pl.chomik.bookshelf.domain.Bibs;

import javax.persistence.EntityManager;
import java.util.Optional;

public class BibsRepositoryImpl implements BibsRepository {

    @Autowired
    private EntityManager entityManager;

    @Override
    public <S extends Bibs> S save(S s) {
        entityManager.persist(s);
        return null;
    }

    @Override
    public <S extends Bibs> Iterable<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<Bibs> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public Iterable<Bibs> findAll() {
        return null;
    }

    @Override
    public Iterable<Bibs> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(Bibs bibs) {

    }

    @Override
    public void deleteAll(Iterable<? extends Bibs> iterable) {

    }

    @Override
    public void deleteAll() {

    }
}
