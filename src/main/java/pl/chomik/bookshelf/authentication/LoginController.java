package pl.chomik.bookshelf.authentication;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMapping;

@Configuration
public class LoginController {

    @RequestMapping("/signing")
    public String authenticate(){
        return "signing";
    }
}
