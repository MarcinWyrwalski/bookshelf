package pl.chomik.bookshelf.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.stereotype.Component;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.validation.constraints.NotNull;
import java.util.Arrays;

@Entity
@Component
@JsonIgnoreProperties(ignoreUnknown = true)
public class Book {

    @NotNull
    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long Id;

    private Bibs[] bibs;

    public Book() {
    }

    public Bibs[] getBibs() {
        return bibs;
    }

    public void setBibs(Bibs[] bibs) {
        this.bibs = bibs;
    }

    @Override
    public String toString() {
        return "Book{" +
                "bibs=" + Arrays.toString(bibs) +
                '}';
    }
}
