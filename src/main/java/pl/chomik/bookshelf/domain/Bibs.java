package pl.chomik.bookshelf.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.context.annotation.Bean;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "bibs")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Bibs {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @NotNull
    private Long id;
    private String title;
    private String author;
    private String isbnIssn;
    private String language;
    private String gerne;

    public String getLanguageOfOriginal() {
        return languageOfOriginal;
    }

    public void setLanguageOfOriginal(String languageOfOriginal) {
        this.languageOfOriginal = languageOfOriginal;
    }

    private String publicationYear;
    private String languageOfOriginal;

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getGerne() {
        return gerne;
    }

    public void setGerne(String gerne) {
        this.gerne = gerne;
    }

    public String getPublicationYear() {
        return publicationYear;
    }

    public void setPublicationYear(String publicationYear) {
        this.publicationYear = publicationYear;
    }


    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getFormOfWork() {
        return formOfWork;
    }

    public void setFormOfWork(String formOfWork) {
        this.formOfWork = formOfWork;
    }

    private String publisher;
    private String formOfWork;





    public Bibs() {
    }

    public Long getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Bibs{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                '}';
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getIsbnIssn() {
        return isbnIssn;
    }

    public void setIsbnIssn(String isbnIssn) {
        this.isbnIssn = isbnIssn;
    }
}
