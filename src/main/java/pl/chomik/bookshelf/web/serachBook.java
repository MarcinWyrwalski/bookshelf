package pl.chomik.bookshelf.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.chomik.bookshelf.domain.Bibs;
import pl.chomik.bookshelf.domain.Book;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

@Controller
class serachBook {

    @RequestMapping("/serachBook")
    public String serachBook(@RequestParam("ISBN") String str, Model model) throws IOException, URISyntaxException {

        URIBuilder uriBuilder = new URIBuilder("http://data.bn.org.pl/api/bibs.json?");
        uriBuilder.addParameter("isbnIssn", str);
        URL url = uriBuilder.build().toURL();

//        System.out.println(url);

        ObjectMapper mapper = new ObjectMapper();
        Book book = mapper.readValue(new URL(url.toString()), Book.class);

        Bibs[] bibs = book.getBibs();


        model.addAttribute("a", bibs[0].getAuthor());
        model.addAttribute("b", bibs[0].getTitle());
        model.addAttribute("c", bibs[0].getId());
        model.addAttribute("d", bibs[0].getIsbnIssn());


        return "resultPage";
    }

}
